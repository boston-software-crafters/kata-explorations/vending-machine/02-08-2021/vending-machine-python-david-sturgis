import pytest
from vending_machine.money import Coin, Dollar
from vending_machine.vending_machine import VendingMachine, ChangeBin, MainDisplay


class TestVendingMachine:
    def test_empty_vending_machine_displays_insert_coin(self):
        machine = VendingMachine(Dollar(0.0))
        assert machine.display_value() == MainDisplay("INSERT COIN")

    def test_non_empty_vending_machine_displays_dollar_value(self):
        machine = VendingMachine(Dollar(1.23))
        assert machine.display_value() == MainDisplay("$1.23")

    def test_rounding(self):
        machine = VendingMachine(Dollar(1.23001))
        assert machine.display_value() == MainDisplay("$1.23")

    def test_empty_machine_get_change(self):
        machine = VendingMachine()
        assert machine.get_change() == ChangeBin([])

    def test_nickel_to_dollar(self):
        assert Coin.NICKEL.to_dollar() == Dollar(0.05)

    def test_dime_to_dollar(self):
        assert Coin.DIME.to_dollar() == Dollar(0.10)

    def test_quarter_to_dollar(self):
        assert Coin.QUARTER.to_dollar() == Dollar(0.25)

    @pytest.mark.parametrize("coin", list(Coin))
    def test_insert_coin_goes_to_get_change(self, coin):
        machine = VendingMachine()
        machine.insert_coin(coin)
        coin_string = str(coin.to_dollar())
        assert machine.display_value() == MainDisplay(coin_string)

    def test_insert_coin_multiple_coins(self):
        machine = VendingMachine()
        first_coin = Coin.DIME
        second_coin = Coin.NICKEL
        machine.insert_coin(first_coin)
        machine.insert_coin(second_coin)
        assert machine.display_value() == MainDisplay("$0.15")

    @pytest.mark.parametrize("first_coin", list(Coin))
    @pytest.mark.parametrize("second_coin", list(Coin))
    def test_insert_two_accepted_coins(self, first_coin, second_coin):
        # TODO if you are using Cent, you need to update this test.
        dollars_values = {
            Coin.DIME: Dollar(0.10),
            Coin.NICKEL: Dollar(0.05),
            Coin.QUARTER: Dollar(0.25),
        }
        expected_dollars = dollars_values[first_coin].plus(dollars_values[second_coin])
        # format as fixed point to two decimal places
        expected_string = f"${expected_dollars.value:.2f}"

        machine = VendingMachine()
        machine.insert_coin(first_coin)
        machine.insert_coin(second_coin)

        assert machine.display_value() == MainDisplay(expected_string)
