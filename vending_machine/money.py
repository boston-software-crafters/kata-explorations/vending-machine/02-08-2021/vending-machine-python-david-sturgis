from dataclasses import dataclass
from enum import Enum, auto


class Coin(Enum):
    NICKEL = auto()
    DIME = auto()
    QUARTER = auto()

    def __lt__(self, other):
        return self.name < other.name

    def to_dollar(self):
        return {
            Coin.NICKEL: Dollar(0.05),
            Coin.DIME: Dollar(0.10),
            Coin.QUARTER: Dollar(0.25),
        }[self]


@dataclass(frozen=True, order=True)
class Dollar:
    value: float

    def __str__(self):
        """Useful python formatting here:
        https://docs.python.org/3/library/string.html#format-specification-mini-language """
        return f"${self.value:.2f}"

    @classmethod
    def from_coin(cls, coin: Coin) -> "Dollar":
        raise NotImplementedError

    def plus(self, other: "Dollar") -> "Dollar":
        return Dollar(self.value + other.value)

    def minus(self, other: "Dollar") -> "Dollar":
        return Dollar(self.value - other.value)

    def __add__(self, other):
        return Dollar(self.value + other.value)

@dataclass(frozen=True, order=True)
class Cent:
    value: int

    def __str__(self):
        raise NotImplementedError
