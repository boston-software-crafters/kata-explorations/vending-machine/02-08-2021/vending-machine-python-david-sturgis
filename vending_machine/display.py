from dataclasses import dataclass
from typing import Iterable

from vending_machine.product import ProductKey


@dataclass
class SingleProductDisplay:
    name: str
    price: str
    key: ProductKey


@dataclass
class AllProductsDisplay:
    _products: Iterable[SingleProductDisplay]

    def __iter__(self):
        return sorted(self._products, key=lambda el: el.key)

    def __eq__(self, other):
        if not isinstance(other, AllProductsDisplay):
            return False
        return list(self) == list(other)


@dataclass
class MainDisplay:
    message: str
